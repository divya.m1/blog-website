const userController = require('../controllers/user');
const homeController = require('../controllers/home');
const articleController = require('../controllers/article');
const sendmaill = require('../sendmail');
const imageController = require('../controllers/image.controller');
const imageUploader = require('../helper/image-uploader');
const { createPost } = require('../controllers/article');


//const {createGet} = require('../controllers/article');

module.exports = (app) => {
    app.get('/', homeController.index);
    

    app.get('/user/register', userController.registerGet);
    app.post('/user/register', userController.registerPost)
    app.post('/user/register',sendmail);
    
    
    app.get('/user/login', userController.loginGet);
    app.post('/user/login', userController.loginPost)
    .sendmaill;

    app.get('/user/logout', userController.logout);
   // app.get('/article/update',articleController.updateGet);

    app.get('/article/create',articleController.createGet);
   //  app.post('/article/create', articleController.createPost);

  app.get('/article/update/:id', articleController.updateGet);
   app.get('/article/update/:id',articleController.updatePost);


    app.get('/article/details/:id',articleController.details);
    // app.get('/article/delete2/:id',articleController.deletePost)
    app.get('/article/delete2/:id/:userid',articleController.deletePost);


    

  app.post('/uploads',imageUploader.upload.single('avatar'),articleController.createPost);


   /* app.get('/article/update', function(req, res){
        articleController.create1Get
      });
   */
};

