const express = require('express');
var nodemailer = require('nodemailer');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session');
const passport = require('passport');
const multer = require('multer');
module.exports = (app, config) => {
    app.set('views', (path.join(config.rootFolder, '/public/views')));
    app.set('view engine', 'hbs');
   /* app.use(multer({
        dest : path.join(__dirname, 'public/uploads')
    }).single('avatar'));*/
    var upload = multer({ dest: 'uploads/' })

    app.use(express.static(path.join(config.rootFolder, '/public')));

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));

    app.use(cookieParser('pesho'));

    app.use(session({secret: 'pesho', resave: false, saveUninitialized: false}));

    app.use(passport.initialize());
    app.use(passport.session());

   app.use((req, res, next) => {
        if (req.user) {
            res.locals.user = req.user;
        }
        next();
    });

};
