const express = require('express');
const config = require('./config/config.js');
const app = express();

require('./config/express')(app, config);
require('./config/auth')();
require('./config/routes')(app);
//require('./config/routes')

module.exports = app;


